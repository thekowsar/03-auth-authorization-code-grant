INSERT INTO public.ibuser (ibuseroid,userid,password,role,status,createdon,editedon,currentversion,roleoid) VALUES
('871d0bab-dc9e-448b-a80c-d08821872c6a','md.faisal.hossain','$2a$10$bFe6wh64xVWbG0ghvp/wdOiFsKmRrENu2ODihcF9CVMspNhGOW95C',NULL,'PasswordCreated','2020-03-08 18:33:17.000','2020-03-08 18:33:38.000','1',NULL)
,('16563f99-2987-44b3-b23c-7f7dfsdfewwwrgr','kamrul.hasan.1','$2a$10$eMLj973GUay4DHiFgUwQi.iDMz7JhO6cIjrZThf2Qpo4D65ezXNku',NULL,'PasswordCreated','2020-03-12 10:44:43.416','2020-03-12 10:49:20.922','1',NULL)
,('16563f99-2987-44b3-b23c-7f78147c6890','kamrul.hasan','$2a$10$vW5IA6/VAFenc1BzPx1IuuZELNxgiZ5fRur9VJ51xiEYjAXoqU.ce',NULL,'PasswordCreated','2020-03-12 10:44:43.416','2020-04-26 16:07:39.410','1',NULL)
,('2cbde26e-29b3-4f12-8306-36c45f719dac','erfan.elahi','$2a$10$xUdIAL.8ixRKP8CA9kPld.O3e0/t9fILfuy89kyMz.nREsjlvoIVu',NULL,'PasswordCreated','2020-03-08 18:32:43.000','2020-03-25 12:34:18.000','1',NULL)
,('f9b7acad-d234-4b56-af54-d81ca2a6840a','md.nahidul.islam','$2a$10$LXelQN50QDyNaHcceDKe6er79J1zXRqvB8lo3W1XkSULrTxeaOvmy',NULL,'PasswordCreated','2020-03-11 22:00:10.402','2020-04-08 16:29:39.000','1',NULL)
,('70e75b47-e1df-4bae-a423-9f304696fe82','asif.yusuf','$2a$10$.nf0bz6Acn6ehgk3PyOOwe9Woq4ktlLvKJ2wOnffoDl4ecL7slITm',NULL,'PasswordCreated','2020-03-15 15:49:21.782','2020-04-08 16:29:49.000','1',NULL)
,('fec58c18-c7eb-4689-8a54-3327e5af4838','mushfika.faria','$2a$10$Ct6qxICeWW5IWffo3tEQr.vZ5eFrhcTeQ9XPG.1NS.f5Ov3HW3j2O',NULL,'PasswordCreated','2020-03-22 17:33:13.181','2020-04-05 12:30:34.557','1','ROLE_ADMIN')
,('6afb81a4-85bc-4b87-a462-3d8645b14f73','taslimur.rahaman.737','$2a$10$DXXKx0P/1LI6/mMrH4mVJuDZ3yQ6TQJemUYP2EOTwem4pGqEDAAjG',NULL,'PasswordCreated','2020-03-23 19:01:16.735','2020-03-23 19:05:32.624','1',NULL)
,('814bf09d-5b6b-45fa-b466-ce2eadabdc5d','mohammed.kowsar.rahman','$2a$10$Ct6qxICeWW5IWffo3tEQr.vZ5eFrhcTeQ9XPG.1NS.f5Ov3HW3j2O',NULL,'PasswordCreated','2020-03-12 18:43:54.941','2020-03-24 12:32:50.000','1',NULL)
,('898762d5-d4e8-42c0-98ab-c36a126b0349','md.hafizur.rahman','$2a$10$LdlitVO3EfB3zKpFL7a.CePquXRsV2QTrqb3CZ3cV979IK/gdo5h6',NULL,'PasswordCreated','2020-03-12 10:56:06.107','2020-03-24 13:11:47.101','1',NULL)
;
INSERT INTO public.ibuser (ibuseroid,userid,password,role,status,createdon,editedon,currentversion,roleoid) VALUES
('7b7bcc6d-4a26-4ddf-b4a6-55fa2de1555c','moshiur.rahman','$2a$10$QVvfO3FjfcF9fDvHPSDunuve75nOGwlYT.4iXt3ZzUVWUCCjK/J12',NULL,'PasswordCreated','2020-03-19 16:30:31.181','2020-03-24 15:58:27.000','1',NULL)
,('4c7c7035-016c-40a3-a3de-db18f159e752','taslimur.rahaman','$2a$10$cvHfcrt4l.jOcwtmp/QEv.noISbglPQhWpqBgHRYWlxH0o2/03WWK',NULL,'PasswordCreated','2020-03-12 00:25:18.916','2020-03-24 17:17:15.000','1',NULL)
;

INSERT INTO public.AUTHORITY (authorityOid, authorityName) VALUES (1, 'VIEW_USERNAME'), (2, 'VIEW_HELLO');
INSERT INTO public.ROLE (roleOid, roleName) VALUES (1, 'ADMIN'), (2, 'ENDUSER');

INSERT INTO public.RoleAuthority (authorityId, roleId) VALUES (1, 1);
INSERT INTO public.RoleAuthority (authorityId, roleId) VALUES (2, 1);

INSERT INTO public.RoleAuthority (authorityId, roleId) VALUES (2, 2);

INSERT INTO public.UserRole (userId, roleId) VALUES ('814bf09d-5b6b-45fa-b466-ce2eadabdc5d', 1);
INSERT INTO public.UserRole (userId, roleId) VALUES ('fec58c18-c7eb-4689-8a54-3327e5af4838', 2);