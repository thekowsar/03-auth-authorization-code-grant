package net.celloscope.oauth2;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IbUserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        IbUserEntity user = userRepository.findByUserId(username);
        if (user != null) {
            List<GrantedAuthority> authorities = getUserAuthority(username);
            return buildUserForAuthentication(user, authorities);
        }
        throw new UsernameNotFoundException(username);
    }

    private List<GrantedAuthority> getUserAuthority(String userName) {
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        List<AuthorityEntity> authorityEntities = authorityRepository.getByUserId(userName);
        for(AuthorityEntity authorityEntity : authorityEntities){
            authorities.add(new SimpleGrantedAuthority(authorityEntity.getAuthorityName()));
        }
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(authorities);
        return grantedAuthorities;
    }

    private UserDetails buildUserForAuthentication(IbUserEntity user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getUserId(), user.getPassword(),authorities);
    }
}
