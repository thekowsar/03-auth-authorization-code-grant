package net.celloscope.oauth2;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@ToString
@Table(name="ibuser")
public class IbUserEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ibuseroid", updatable = false, nullable = false)
    private String ibUserOid;

    @Column(name="userid")
    private String userId;

    @Column(name="password")
    private String password;

    @Column(name="role")
    private String role;

    @Column(name="status")
    private String status;

    @Column(name="roleoid")
    private String roleoid;

    @Column(name = "createdon")
    private Timestamp createdOn;

    @Column(name="editedon")
    private Timestamp editedOn;

    @Column(name="currentversion")
    private String currentVersion;

}
